public class CalculationErrorException extends RuntimeException {
    public CalculationErrorException() {
        super("Calculate Error");
    }
}
