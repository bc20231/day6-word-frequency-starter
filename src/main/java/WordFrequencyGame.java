import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class WordFrequencyGame {
    public static final String SPACE = "\\s+";

    public String getResult(String sentence) throws CalculationErrorException {
        List<Input> inputList = countWords(sentence);
        inputList.sort((word1, word2) -> word2.getWordCount() - word1.getWordCount());

        return inputList.stream()
                .map(word -> word.getValue() + " " + word.getWordCount())
                .collect(Collectors.joining("\n"));
    }

    private List<Input> countWords(String inputString) {
        List<String> words = List.of(inputString.split(SPACE));
        HashSet<String> deduplicate = new HashSet<>(words);
        return deduplicate.stream()
                .map(word -> new Input(word, Collections.frequency(words, word)))
                .collect(Collectors.toList());
    }
}
